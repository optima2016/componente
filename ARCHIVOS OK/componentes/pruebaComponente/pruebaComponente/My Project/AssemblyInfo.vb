﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' La información general sobre un ensamblado se controla mediante el siguiente 
' conjunto de atributos. Cambie estos atributos para modificar la información
' asociada con un ensamblado.

' Revisar los valores de los atributos del ensamblado

<Assembly: AssemblyTitle("pruebaComponente")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("OPTIMA S.A.")> 
<Assembly: AssemblyProduct("pruebaComponente")> 
<Assembly: AssemblyCopyright("© OPTIMA S.A. 2014")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'El siguiente GUID sirve como identificador de la biblioteca de tipos si este proyecto se expone a COM
<Assembly: Guid("519b6cfa-fe9a-40bd-8b16-e6a81864a214")> 

' La información de versión de un ensamblado consta de los cuatro valores siguientes:
'
'      Versión principal
'      Versión secundaria 
'      Número de versión de compilación
'      Revisión
'
' Puede especificar todos los valores o establecer como predeterminados los números de versión de compilación y de revisión 
' mediante el asterisco ('*'), como se muestra a continuación:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
