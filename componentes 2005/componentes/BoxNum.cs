using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace componentes
{
    public partial class BoxNum : TextBox
    {
        #region Propiedades BoxModificado
        private comportamiento menuTipoBox;
        private Color ClDefecto;
        private Color ClGotFocus;
        private Color ClLostFocus;
        private int intNroDigitos = 1;
        private Boolean boolLostFocus;
        
        private string sDecimal;
        private string sMiles;

        public Boolean bLostFocus
        {
            get { return boolLostFocus; }
            set { boolLostFocus= value; }
        }
        
        public Color ColorDefecto {
            get { return ClDefecto; }
            set { ClDefecto = value; }
        }
        public Color ColorLostFocus
        {
            get { return ClLostFocus; }
            set { ClLostFocus = value; }
        }
        public Color ColorGotFocus
        {
            get { return ClGotFocus; }
            set { ClGotFocus = value; }
        }
        public enum comportamiento
        {
            Decimal,
            Entero,
            String,
            Codigo
        }
        public int NroDigitos
        {
            get { return intNroDigitos; }
            set { intNroDigitos = value; }
        }
        public comportamiento TipoBox
        {
            get { return menuTipoBox; }
            set { menuTipoBox = value; }
        }
                
        #endregion
        public BoxNum()
        {
            InitializeComponent();
            this.BackColor = ColorDefecto;
            sMiles = CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator;
            //sMiles = CultureInfo.CurrentCulture.NumberFormat.CurrencyGroupSeparator;
            //sDecimal = CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;
            sDecimal = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
        }
        private void verificarDecimal(char key, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(sMiles))
            {
                e.KeyChar = Convert.ToChar(sDecimal);
            }
            if (Char.IsDigit(e.KeyChar) ||  e.KeyChar == Convert.ToChar(sDecimal) || e.KeyChar == '\b') 
            {
                if (e.KeyChar == Convert.ToChar(sDecimal)) {
                    if (this.Text.Contains(Char.ToString(Convert.ToChar(sDecimal))))
                        e.Handled = true;
                    else
                        e.Handled = false;
                }

            }
            else
            {                
                e.Handled = true;
            }
        }
        private void verificarEntero(char key, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar) || e.KeyChar == '\b')
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
        private void verificarString(char key, KeyPressEventArgs e)
        {
             
        }
        private void presionado(object sender, KeyPressEventArgs e)
        {
            switch (menuTipoBox)
            {
                case comportamiento.Decimal:
                    verificarDecimal(e.KeyChar, e);
                    break;
                case comportamiento.Entero:
                    verificarEntero(e.KeyChar, e);
                    break;
                case comportamiento.String:
                    verificarString(e.KeyChar, e);
                    break;
                default:
                    verificarString(e.KeyChar, e);
                    break;
            }
        }
        private void GotFoco(object sender, EventArgs e) {
            bLostFocus = false;
            this.BackColor = ColorGotFocus;
            this.SelectAll();
        }
        private void LostFoco(object sender, EventArgs e) {
            this.BackColor = ColorLostFocus;
            bLostFocus = true;
            switch (menuTipoBox)
            {
                case comportamiento.Decimal:
                    if (this.Text == "")
                        this.Text = "0"+sDecimal+"0";

                    if (this.Text == sDecimal)
                        this.Text = "0"+sDecimal+"";

                    if ((this.Text.IndexOf(sDecimal)+1) == this.Text.Length)
                        this.Text = this.Text+"0";

                    if (this.Text.IndexOf(sDecimal) < 0)
                        this.Text = this.Text + sDecimal + "0";

                    this.Text = Convert.ToString(Decimal.Round(Convert.ToDecimal(this.Text),this.NroDigitos));
                    this.Text = generarCero(this.Text); 
                    break;
                case comportamiento.Entero:
                                        
                    break;
                case comportamiento.String:
                    break;
                default:
                    break;
            }            
        }
        private string generarCero( string text ){
            int i = 0;
            int index = text.IndexOf(sDecimal)+1;
            i = text.Length - index;
            i = NroDigitos - i;

            while (i > 0 )
            {
                text = text + "0";
                i--;
            }
            return text;
        }
    }
}