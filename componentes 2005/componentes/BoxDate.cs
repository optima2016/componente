﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.Text;

namespace componentes
{
    public partial class BoxDate : MaskedTextBox
    {
        #region Propiedades BOX
        private Color ClDefecto;
        private Color ClGotFocus;
        private Color ClLostFocus;

        public Color ColorDefecto
        {
            get { return ClDefecto; }
            set { ClDefecto = value; }
        }
        public Color ColorLostFocus
        {
            get { return ClLostFocus; }
            set { ClLostFocus = value; }
        }
        public Color ColorGotFocus
        {
            get { return ClGotFocus; }
            set { ClGotFocus = value; }
        }
        #endregion
        public BoxDate()
        {
            InitializeComponent();
            this.BackColor = ColorDefecto;
        }
        private void LostFoco(object sender, EventArgs e)
        {
            this.BackColor = ColorLostFocus;
        }
        private void GotFoco(object sender, EventArgs e)
        {
            this.BackColor = ColorGotFocus;
        } 
    }
}
