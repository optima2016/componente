using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace componentes
{
    public partial class BoxCombo : ComboBox
    {
        #region Propiedades BOX
        private Color ClDefecto;
        private Color ClGotFocus;
        private Color ClLostFocus;        

        public Color ColorDefecto
        {
            get { return ClDefecto; }
            set { ClDefecto = value; }
        }
        public Color ColorLostFocus
        {
            get { return ClLostFocus; }
            set { ClLostFocus = value; }
        }
        public Color ColorGotFocus
        {
            get { return ClGotFocus; }
            set { ClGotFocus = value; }
        }
        #endregion
        public BoxCombo()
        {
            InitializeComponent();
            this.BackColor = ColorDefecto;
        }
        private void LostFoco(object sender , EventArgs e){
            this.BackColor = ColorLostFocus;
        }
        private void GotFoco(object sender, EventArgs e){
            this.BackColor = ColorGotFocus;
        } 
    }
}
